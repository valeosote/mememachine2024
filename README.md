# Meme Machine 2024

Tervetuloa meemien maailmaan! Tästä repositoriosta löydät fullstack-projektimme.

Dokumetointia löytyy [documents](./documents)-kansiosta.
Siellä on mm. [projektisuunnitelma](./documents/README.md) ja [käyttöliittymän suunnitelma](./documents/suunnitelma.md).

# Pikaesittely sivuston toiminnosta

[![MemeMachine](https://img.youtube.com/vi/EkEOAkdbLnk/0.jpg)](https://youtu.be/EkEOAkdbLnk)

