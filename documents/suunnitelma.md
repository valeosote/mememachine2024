Seuraavat kuvauksia selventää käyttöliittymän hahmottelu, joka löytyy täältä:
https://ninjamock.com (vaati sisäänkirjautumisen, ja oikeudet projektiin)

# Näkymät ja niissä olevat toiminnot:

## Näkymä 1. Pääsivu:

- Headerissa näkyy logo ja valikossa linkki Koti. Valikko muodostetaan komponentissa **”Menu”**.

- Sisältöosa on jaettu kolmeen osaan seuraavalla tavalla:
	- Kaksi osaa komponentista **”MainPage”**:  
		- Kahden esimerkkiteeman tiedot saadaan komponentista **”NewestThemeMemes”** (tuodaan parent-komponentin sisälle):
		- Vasemmalla on ensimmäinen esimerkkiteeman uusin meemi ja keskellä on toisen esimerkkiteeman uusin meemi
		(näytettävät tiedot ovat: Teeman nimi, teeman kuva ja meemiteksti)
	- Yksi osa komponentista **"DynamicZone"**:
		- Oikealla on tervetuloa-teksti ja painikkeet Rekisteröidy ja Kirjaudu (saadaan komponentista **"Welcome"**)

	Painikkeesta ”Register” siirrytään näkymään nro 2.
	Painikkeesta ”Login” siirrytään näkymään nro 3.

## Näkymä 2: Rekisteröinti

- Headerissa näkyy logo ja valikossa linkki ”Koti”.  Valikko muodostetaan komponentissa **”Menu”**.

- Sisältöosa on jaettu kolmeen osaan seuraavalla tavalla:
	- Kaksi osaa komponentista **”MainPage”**:  
		- Kahden esimerkkiteeman tiedot saadaan komponentista **”NewestThemeMemes”** (tuodaan parent-komponentin sisälle):
		- Vasemmalla on ensimmäinen esimerkkiteeman uusin meemi ja keskellä on toisen esimerkkiteeman uusin meemi
		(näytettävät tiedot ovat: Teeman nimi, teeman kuva ja meemiteksti)
	- Yksi osa komponentista **"DynamicZone"**:
		- Oikealla näkyy rekisteröitymislomake, joka saadaan komponentista **"Register"**
		- Lomakkeessa on teksti "Rekisteröidy käyttäjäksi", kentät nimelle ja salasanalle sekä "Lähetä" painike

Painikkeesta ”Register” siirrytään näkymään nro 4.

## Näkymä 3: Sisäänkirjautuminen

- Headerissa näkyy logo ja valikossa linkki ”Koti”.  Valikko muodostetaan komponentissa **”Menu”**.

- Sisältöosa on jaettu kolmeen osaan seuraavalla tavalla:
	- Kaksi osaa komponentista **”MainPage”**:  
		- Kahden esimerkkiteeman tiedot saadaan komponentista **”NewestThemeMemes”** (tuodaan parent-komponentin sisälle):
		- Vasemmalla on ensimmäinen esimerkkiteeman uusin meemi ja keskellä on toisen esimerkkiteeman uusin meemi
		(näytettävät tiedot ovat: Teeman nimi, teeman kuva ja meemiteksti)
	- Yksi osa komponentista **"DynamicZone"**:
		- Oikealla näkyy rekisteröitymislomake, joka saadaan komponentista **"Signin"**
		- Lomakkeessa on teksti "Kirjaudu sisään", kentät nimelle ja salasanalle sekä "Lähetä" painike

Painikkeesta Lähetä siirrytään näkymään nro 4.


## Näkymä 4: Sisäänkirjautumisen jälkeen

- Headerissa näkyy logo ja valikossa linkit ”Home”, ”Hae meemiä” ja ”Lisää meemi”.  Valikko muodostetaan komponentissa **”Menu”**.

- Headerissa näkyy oikeassa laidassa kirjautuneen käyttäjän nimi ja sen oikealla puolella painike ”LOGOUT”.

- Sisältöosa on jaettu kahteen osaan seuraavalla tavalla:

	- Vasemmalla on ensimmäinen tietokantaan lisätty uusin meemi, joka saadaan komponentista **NewestThemeMemes**.
		(näytettävät tiedot ovat: Teeman nimi, teeman kuva ja otsikon esim ”teeman uusin meemi”: alla meemiteksti) KORJAA TÄMÄ!
	- Oikealla on tervetuloateksti ”Terve vaan, käyttäjä. Valitse toiminto valikosta. Hyvää meemittelyä!” 
		(tulee komponentista **"Familiar"**)


## Näkymä 5: Hakusivu

- Headerissa näkyy logo ja valikossa linkit ”Koti”, ”Hae meemiä” ja ”Lisää meemi”. Valikko muodostetaan komponentissa **”Menu”**.
- Headerissa näkyy oikeassa laidassa kirjautuneen käyttäjän nimi ja sen oikealla puolella painike ”LOGOUT”

-Sisältöosa muodostuu otsikosta sekä hakuvalinnoista: 
	1. Painikkeet joilla voi valita halutun teeman
	2. Painike jolla haetaan 10 uusinta meemiä
	3. Painike jolla haetaan kaikki meemit
	(tulevat komponenetista **"FetchPage"**)

Hakuvaihtoehdosta 1 siirrytään näkymään 6.
Hakuvaihtoehdosta 2 siirrytään näkymään 7.
Hakuvaihtoehdosta 3 siirrytään näkymään 8.

## Näkymä 6: Valittu hakusivuilta vaihtoehto 1 ja näytetään valitun teeman kaikki meemit

- Headerissa näkyy logo ja valikossa linkit ”Koti”, ”Hae meemiä” ja ”Lisää meemi”.  Valikko muodostetaan komponentissa **”Menu”**.
- Headerissa näkyy oikeassa laidassa kirjautuneen käyttäjän nimi ja sen oikealla puolella painike ”LOGOUT”.

- Sisältöosa on jaettu seuraavasti:
	- Headerin alapuolella näkyvät hakuvalinnat:
		1. Painikkeet joilla voi valita halutun teeman
		2. Painike jolla haetaan 10 uusinta meemiä
		3. Painike jolla haetaan kaikki meemit
	- Teeman mukaiset meemit näytetään hakuvalintojen alapuolella, kaksi meemiä rinnakkain. 
	- Jokainen meemi on omassa div-elementissään, joka sisältää teeman mukaisen kuva, tekstin sekä tykkäyspainikkeen.
		(Tiedot tulevat komponentista **"FetchMemesByTheme"**)


## Näkymä 7: Valittu hakusivuilta vaihtoehto 2 ja näytetään vain uusimmat meemit

- Headerissa näkyy logo ja valikossa linkit ”Koti”, ”Hae meemiä” ja ”Lisää meemi”.  Valikko muodostetaan komponentissa **”Menu”**.
- Headerissa näkyy oikeassa laidassa kirjautuneen käyttäjän nimi ja sen oikealla puolella painike ”LOGOUT”.

- Sisältöosa on jaettu seuraavasti:
	- Headerin alapuolella näkyvät hakuvalinnat:
		1. Painikkeet joilla voi valita halutun teeman
		2. Painike jolla haetaan 10 uusinta meemiä
		3. Painike jolla haetaan kaikki meemit
	- Teeman mukaiset meemit näytetään hakuvalintojen alapuolella, kaksi meemiä rinnakkain. 
	- Jokainen meemi on omassa div-elementissään, joka sisältää teeman mukaisen kuva, tekstin sekä tykkäyspainikkeen.
		(Tiedot tulevat komponentista **"FetchLatestMemes"**)


## Näkymä 8: Valittu hakusivuilta vaihtoehto 3 ja haetaan kaikki tietokannassa olevat meemit

- Headerissa näkyy logo ja valikossa linkit ”Koti”, ”Hae meemiä” ja ”Lisää meemi”.  Valikko muodostetaan komponentissa **”Menu”**.
- Headerissa näkyy oikeassa laidassa kirjautuneen käyttäjän nimi ja sen oikealla puolella painike ”LOGOUT”.

- Sisältöosa on jaettu seuraavasti:
	- Headerin alapuolella näkyvät hakuvalinnat:
		1. Painikkeet joilla voi valita halutun teeman
		2. Painike jolla haetaan 10 uusinta meemiä
		3. Painike jolla haetaan kaikki meemit
	- Teeman mukaiset meemit näytetään hakuvalintojen alapuolella, kaksi meemiä rinnakkain. 
	- Jokainen meemi on omassa div-elementissään, joka sisältää teeman mukaisen kuva, tekstin sekä tykkäyspainikkeen.
		(Tiedot tulevat komponentista **"FetchAllMemes"**)


## Näkymä 9: Uuden meemin postaus

- Headerissa näkyy logo (joka on myös linkki pääsivulle) ja valikossa linkit ”Home”, ”Hae meemiä” ja ”Lisää meemi”.  Valikko muodostetaan komponentissa ”Menu”.

- Headerissa näkyy oikeassa laidassa kirjautuneen käyttäjän nimi ~~ja sen oikealla puolella painike ”LOGOUT”~~.

- Sisältöosa on jaettu kahteen riviin: 
	- Ensimmäisellä rivillä on otsikko ”Lisää uusi meeni”.
	- Toinen rivi on jakautunut kahteen osaan:
		- Vasemmalla puolella on valittavan teeman tiedot 
		- oikealla on lisäyslomake, jossa on tekstikenttä ja painike "Lähetä".

- Vasemmalla puolella oleva teeman valinta tehdään klikkaamalla kuvaa, valittu kuva näkyy sivun alareunassa.

## Näkymä 10: Logout

- Headerissa näkyy logo ja valikossa linkit ”Koti”, ”Hae meemiä” ja ”Lisää meemi”.  Valikko muodostetaan komponentissa **”Menu”**.
- Headerissa näkyy oikeassa laidassa kirjautuneen käyttäjän nimi ja sen oikealla puolella painike ”LOGOUT” 

- Logout-nappi palauttaa koko näkymän alkuperäiseen, kirjautumattomaan versioon. Eli kun käyttäjä on kirjautunut ulos, siirrytään näkymään 1: Pääsivu.

# React komponentit

- Main
- App
  - Menu
  - MainPage
    - NewestThemeMemes
  - DynamicZone
    - Welcome
    - Register
    - Signin
    - Familiar
  - FetchPage
    - FetchMemesByTheme
    - FetchLatestMemes
    - FetchAllMemes
  - PostMeme
- ErrorPage

