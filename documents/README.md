# Projektin Suunnitteludokumentti: MemeMachine

Tekijät:

- Anssi Kuusniemi
- Mirva  Näivö
- Miika Toukola
- Neljäs Tiimiläinen

## Kuvaus

Projektin tarkoitus on luoda selaimen kautta käytettävä fullstack sovellus.
Käyttäjät voivat katsoa ja jakaa meemitekstejä joihin liittyy kuva.

## Toimintoja

1. sovelluksen pääsivu
2. käyttäjän rekisteröinti
3. käyttäjän kirjautuminen
4. meemin lisääminen (eli meemitekstin lähettäminen)
5. meemien haku
6. meemien peukutus

### Bonus 

- valikko -> toteutettu
- käyttäjätietojen päivittäminen
- meemien muokkaus / poistaminen
- uusimmat meemit -näkymä -> toteutettu
- yksittäisen meemin oma näkymä
- meemien kommentointi
- meemien tagaaminen
- yhdellä meemillä useampi tagi
- erilaiset tekstiominaisuudet (sijainti, fontti)
- muita hakutuloksia, esim eniten tykkäyksiä, usampi teema valittuna
- käyttäjän oman kuvan lataaminen

### Käyttäjäryhmien oikeudet

#### Kirjautumattoman käyttäjän toiminnot

1. sovelluksen pääsivu
2. käyttäjän rekisteröinti
3. käyttäjän kirjautuminen

#### Kirjautumisen vaativat toiminnot

4. meemin lisääminen
5. meemien haku
6. meemien peukutus
7. ulos kirjautuminen

## Sovelluksen näkymät

- pääsivu
  - nappi -> rekisteröitymislomake
  - nappi -> kirjautumislomake
  - uusimmat meemit kahdesta teemasta
- kirjautumisen jälkeen näkyvä etusivu
  - käyttäjänimi
  - uusimmat meemit kahdesta teemasta
  - valikko
  - meemin lisääminen
- teeman valinnan jälkeinen näkymä
  - haun tulokset
    * meemiteksti, kuva, tykkäykset
  - hakuvaihtoehdot
    * teeman valinta
    * uusimmat meemit
    * kaikki meemit
- hakusivu meemitekstin perusteella (?)
- yksittäisen meemin katsominen (zoomaus yhteen?) (?)
    * kommentit
- kaikkien tagien tarkastelu (?)

### Sovelluksen näkymien palikat

- pääsivu
- sivupaneeli
  * tervetulotervehdys
  * rekisteröitymislomake
  * kirjautumislomake
  * sisäänkirjautumistervehdys
- meemin lisääminen
- uusin meemi (?)
- valikko
- haku
- pudotusvalikko jossa teemat (?)
- käyttäjänimi (?)
- lista haun tuloksista

## Tekninen toteutus

### GIT
  - Yhteinen dev-haara, josta toimivat osat mainiin
  - Henkilökohtaiset dev-haarat, joista muutokset dev-haaraan
  - Tekijä laittaa pushatessa jollekin tiimiläiselle tarkistettavaksi -> tiimiläinen hyväksyy -> tekijä laittaa muutoksen eteenpäin

### Reitit
 - GET /mememachine/ -> palauttaa kaikki meemit
 - GET /mememachine/id/:id -> hakee meemin id:n perusteella
 - GET /mememachine/theme/:themeId -> palauttaa tietyn teeman meemit (hakutoiminto)
 - GET /mememachine/latestMemes -> hakee uusimmat meemit (hakutoiminto)
 - GET /mememachine/theme/latest/:selectedTheme -> hakee valitun teeman viimeksi lisätyn meemin (hakutoiminto)
 - POST /mememachine/register -> käyttäjän rekisteröityminen
 - POST /mememachine/login -> käyttäjän sisäänkirjautuminen
 - POST /mememachine/post -> uuden meemin julkaiseminen
 - POST /mememachine/id/like/:memeId -> Kasvattaa valitun meemin tykkäysten määrää yhdellä (sama henkilö voi tykätä useita kertoja)

### Frontend
 - käytetään Reactia
   * React Router käyttöön, mutta vain error pagen näyttämistä varten

### Backend
  - Reitit
  - Dao
  - Tietokanta
  - testit

### Rekisteröinti, kirjautuminen ja autentikointi

  Frontendin (eli Reactin) puolella käyttäjä rekisteröityy syöttämällä nimen ja salasanan. Nimeksi hyväksytään 2-200 merkkiä pitkä string, ja salasanksi 6-200 merkkiä pituinen string. Kun käyttäjä painaa lähetä-nappia, nämä tiedot lähetetään fetch-komennolla JSON-muodossa backendille (/mememachine/register). Backendin puolella tiedot otetaan vastaan ja tallennetaan omiin muuttujiin. Ensiksi tarkistetaan löytyykö käyttäjänimi jo tietokannasta, ja jos löytyy, niin rekisteröityminen keskeytetään. (Tällöin käyttäjän näkymässä ei tapahdu muutosta.) Jos käyttäjänimi on vapaana, salasanasta muodostetaan tarkiste (hash) joka tallennetaan käyttäjänimen kanssa tietokantaan. Tietokanta palauttaa siellä muodostuneen käyttäjäkohtaisen user_id luvun. Seuraavaksi käyttäjänimen ja user_id perusteella (ja käyttäen salaista ympäristömuuttujaa SECRET) luodaan tunnistin (token). Tämä tokeni palautetaan frontendille, taas JSON muodossa. Tässä vaiheessa sekä käyttäjänimi ja token tallennetaan frontendin puolelle (nämä ovat useState muuttujina App-komponentissa, ja pääsy niihin Register-komponentista on mahdollistettu propsien kautta). Frontend tunnistaa käyttäjän olevan kirjautunut ja vaihtaa näkymän tervetulotoivotukseen. Yläbanneriin ilmestyy näin näkyviin tarjolla olevia toimintoja ja myös käyttäjän nimi, joka ilmaisee hänen olevan kirjautunut sisään.

  Kirjautuminen tapahtuu hyvin samalla tavalla kuin rekisteröinti, ainakin frontin puolella. Bäkkärillä kun käyttäjänimi löytyy tietokannasta, tarkistetaan salasanan oikeellisuus käyttämällä sen tarkistetta (hash). Tämän onnistuessa, luodaan myös token joka palautetaan frontille. (Huom. tämä token ei ole kirjaimellisesti sama string kuin kirjautuessa, mutta sen sisältämät tiedot ovat. Tokenin tiedot pystyy ulkopuolinen lukemaan, mutta ulkopuoliset eivät voi luoda vastaavaa tokenia tuntematta backendin "salaisuutta".)

  Frontend siis tunnistaa käyttäjän kirjautuneeksi jos muistiin on tallennettu token. Tämän lisäksi muistissa on käyttäjänimi, joka otettiin talteen kirjautumisvaiheessa, jotta sen voi näyttää käyttäjälle muistutuksena siitä, että hän on kirjautunut. (Frontend ei käytä käyttäjän id:tä.) Tätä tokenia tarvitaan taas siinä vaiheessa kun käyttäjä valitsee suojatun (autentikointia vaativan) toiminnon, eli käytännössä tahtoo nähdä jotain tietoa joka on tallennettu tai yrittää tallentaa uutta tietoa tietokantaan. Esimerkkinä tästä kirjattakoon vielä tarkempi selvitys kuinka uuden meemin postaaminen tapahtuu.

  Kun käyttäjä tulee Postaa meemi -näkymään, hän voi valita tarjolla olevista kuvista yhden klikkaamalla. Sitten hän voi kirjoittaa tekstin joka yhdessä kuvan kanssa muodostavat hulvattoman meemin. Kun käyttäjä painaa nappia, frontend muodostaa fetch-haun backendille (/mememachine/post), joka koostuu JSON-muotoisesta tiedosta (meme_text & theme) yhdistettynä tokenin kanssa (headerissä). Backend ottaa tiedot vastaan ja alkuun tarkistaa onko token luotu käyttäen oikeaa salaisuutta ja, jos on, tallentaa tokenin kantamat tiedot helpommin käsiteltäviin muutujiin (decodedToken.userId). Tietokantaan tallennetaan tiedot uudesta meemistä jossa on mukana myös käyttäjän userId, jotta myöhemmin tiedetään kuka meemin on luonut. (Tässä olisi voinut käyttää myös käyttäjänimeä viittauksena tekijään koska tietokannan käyttäjänimet ovat uniikkeja, mutta id käyttämäminen on parempaa tietokantakäytäntöä. Myöskään koko tokenia ei tietenkään tallenneta tietokantaan.) Näin on uusi meemi luotu ja käyttäjä voi mennä katsomaan luomustaan hakusivulta.

### Kuvat

 Tallennetaan kuvat kansiorakenteeseen valmiiksi. Tavoitteena 5-20 kuvaa joihin sitten voi liittää tekstiä.

### Tietokanta

Taulut:

- käyttäjät
  - user_id
  - tunnus
  - salasanahash
- meemit (teksti)
  - meme_id
  - user_id (meemin luoja)
  - teksti
  - aika
  - peukutuslukumäärä
  - theme (liittyy siihen, mikä kuva näytetään)
- tagit ?
  - tag_id
  - meme_id
- kommentit ?
  - comment_id
  - teksti
  - aika
  - meme_id
  - user_id
- välitaulu ?
  - meme_id
  - tag_id


## Aikataulutus

- Perjantai: suunnittelua
- Maanantai: koodit käyntiin
- Tiistai: koodit kasaan
- Keskiviikko: koodit kehitykseen
- Torstai: ulkoasua ja palasten parantelua
- Perjantai: viimeistelyä ja esitykseen valmistautumista
- Maanantai: projektin esittely

## Roolit ja vastuut ja taskit ja tekemistä ja syytettäviä

Projektia alkuun:

0. Tietokantaserverin luominen Azureen - Miika - done
1. Tietokannan alustus - Mirva - done
2. Frontin React-perusrungon teko - Neljäs - done
3. Backendin perusrungon teko - Anssi - done

Toimintoja: 

4. Frontin etusivun rakentaminen - Neljäs - done
5. Uusimmat meemit pääsivulla näkyvistä teemoista (frontend + backend) - Neljäs - done
6. Rekisteröityminen ja kirjautuminen - Miika - done
7. Meemin postaus - Anssi - done
8. Meemien haku uusinta lukuunottamatta - Mirva - done
9. Autentikointi - osassa komponentteja
10. Testit (backend)
11. React rakenteen järjestely olemassa olevien palojen perusteella - Mirva - done
12. Meemien tykkääminen - Neljäs - done

Useampia pienempiä osia tehty merkkaamatta tänne. Tässä vielä torstaina mietittyjä pienempiä nakkeja:

- peukutustoiminto
- dockerfile
- kuvat eri kansioon
- ulkoasun parantelua
- bugi uloskirjautumisessa postaus- ja hakunäkymistä
- Azure app

Kaikki yllä olevat nakit tehty!

## Haasteet ja Ongelmat

- teknisellä puolella autentikointi koski monta palasta ja niiden yhteensovittaminen vaati aivotyöskentelyä
- sekalaisia pieniä haasteita (yhteisen kommunikointityylin löytäminen, aikataulutus)
- kommunikointi kokonaisuudesta

## Arviointi

Osasimme valita sopivan kokoisen projektin toteutettavaksi viikossa. Projekti sujui hyvin eteenpäin vaikka asiaankuuluvia ongelmia tuli lähes joka kohdassa vastaan. Pääsimme jo hyvin kärryille toistemme toimintatavoista ja kuinka yhteistä projektia hallinnoidaan. Toimitimme toimivan tuotteen joka vastasi tavoitteita hyvin.

