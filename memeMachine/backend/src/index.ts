import express from 'express'
import 'dotenv/config'
import router from './routes'

const server = express()
server.use(express.json())
server.use('/', express.static('./dist/client'))
server.use('/mememachine', router)

const PORT = process.env.PORT || 3000

server.listen(PORT, () => {
    console.log(PORT)
})

export default server