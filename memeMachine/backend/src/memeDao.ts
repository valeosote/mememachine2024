import { executeQuery } from './db'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const postMeme = async (post: any) => {
    const query = 'INSERT INTO memes (meme_text, theme, user_id) VALUES ($1, $2, $3);'
    const body = Object.values(post)
    console.log(body)
    const result = await executeQuery(query, body)
    return result
}

export default { postMeme }

interface Meme {
    meme_id: number;
    meme_text: string;
    meme_time: Date;
    likes: number;
    theme: string;
    user_id: number | null;
}

export const getMemes = async () => {
    const query = 'SELECT * FROM memes'
    const result = await executeQuery(query)
    return result.rows
}

export const getMemeById = async (id: number) => {
    const query = 'SELECT * FROM memes WHERE meme_id = $1'
    const result = await executeQuery(query, [id])
    return result.rows
}

export const getMemesByTheme = async (theme: string) => {
    const query = 'SELECT * FROM memes WHERE theme = $1'
    const result = await executeQuery(query, [theme])
    return result.rows
}

export const getLatestMemes = async () => {
    const query = 'SELECT * FROM memes ORDER BY meme_time DESC LIMIT 10'
    const result = await executeQuery(query)
    return result.rows
}

export const getTheLatestMemeOfTheTheme = async (theme: string) => {
    try {
        const query = 'SELECT * FROM memes WHERE theme = $1 ORDER BY meme_time DESC LIMIT 1'
        const result = await executeQuery(query, [theme])

        if (result.rows.length === 0) {
            console.log('No memes found for the specified theme')
            return null
        }

        const latestMeme: Meme = result.rows[0]
        return latestMeme
        
    } catch (error) {
        console.error('Error fetching latest meme:', error)
        throw error
    }
}

const getUser = async (username: string) => {
    const query = 'SELECT * FROM users WHERE username = $1;'
    const parameters = [username]
    const result = await executeQuery(query, parameters)
    if (result && result.rows && result.rows.length > 0) {
        return result.rows[0]
    } else {
        return null
    }
}

const createUser = async (username: string, passwordHash: string) => {
    const query = 'INSERT INTO users (username, passwordhash) VALUES ($1, $2) RETURNING user_id;'
    const parameters = [username, passwordHash]
    const result = await executeQuery(query, parameters)
    if (result.rows && result.rows.length > 0) {
        const newUserId: string = result.rows[0].user_id
        return newUserId
    } else {
        console.error('No rows returned after user creation.')
        return null
    }
}

export { getUser, createUser }