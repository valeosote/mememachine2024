import express, { Request, Response } from 'express'
import dao from './memeDao'
import 'dotenv/config'
import argon2 from 'argon2'
import jwt from 'jsonwebtoken'
import { getUser, createUser } from './memeDao'
import { getMemes, getMemeById, getTheLatestMemeOfTheTheme, getMemesByTheme, getLatestMemes } from './memeDao'
import { authenticate, CustomRequest } from './middleware'
import { executeQuery } from './db'

const router = express.Router()

router.get('/', authenticate, async (_req: Request, res: Response) => {
    const result = await getMemes()
    res.send(result)
})

router.get('/id/:id', async (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const result = await getMemeById(id)
    res.send(result[0])
})

// Adds a new like to the selected meme
router.post('/id/like/:memeId', async (req: Request, res: Response) => {

    try {
        const memeId = Number(req.params.memeId)
        const query = 'UPDATE memes SET likes = likes + 1 WHERE meme_id = $1 RETURNING *'
        const result = await executeQuery(query, [memeId])

        if (result.rowCount === 0) {
            return res.status(404).json({ error: 'Meme not found' })
        }

        return res.status(200).json({ message: 'Like saved!' })

    } catch (error) {
        console.error('Error liking meme:', error)
        return res.status(500).json({ error: 'Internal server error' })
    }
})

router.get('/theme/:theme', async (req: Request, res: Response) => {
    const theme = req.params.theme
    const result = await getMemesByTheme(theme)
    res.send(result)
})

router.get('/latestMemes', async (req: Request, res: Response) => {
    const result = await getLatestMemes()
    res.send(result)
})

router.get('/theme/latest/:selectedTheme', async (req: Request, res: Response) => {
    const selectedTheme = req.params.selectedTheme
    try {
        const result = await getTheLatestMemeOfTheTheme(selectedTheme)
        if (result) {
            res.json(result)
        } else {
            res.status(404).json({ message: 'No meme found for the selected theme' })
            console.log('No latest meme found for the selected theme:', selectedTheme)
        }
    } catch (error) {
        console.error('Error reading latest meme:', error)
        res.status(500).json({ message: 'Internal server error' })
    }
})

//Register a new user
router.post('/register', async (req: Request, res: Response) => {
    const { username, password } = req.body

    if (!username || !password) {
        res.status(400).send('Please provide a username and password.')
    }

    const findUser = await getUser(username)
    if (findUser !== null) {
        res.status(400).send('Invalid name, please use another.')
    } else {
        
        const hashedPassword = await argon2.hash(password)
        const userId = await createUser(username, hashedPassword)
        
        const payload = { username, userId }
        const secret = process.env.SECRET ?? 'salaisuus'
        const token = jwt.sign(payload, secret)

        res.status(201).send({ token })
    }
})

//Login
router.post('/login', async (req: Request, res: Response) => {
    const { username, password } = req.body

    const user = await getUser(username)
    if (user === null) {
        res.sendStatus(401)
    }

    const hash = user.passwordhash
    const passwordMatchesHash = await argon2.verify(hash, password)

    if (passwordMatchesHash) {    
        console.log('Login done')

        const userId = user.user_id
        const payload = { username, userId }
        const secret = process.env.SECRET ?? 'salaisuus'
        const token = jwt.sign(payload, secret)

        res.status(200).send({ token })
    } else {
        res.sendStatus(401)
    }
})

router.post('/post', authenticate, async (req: CustomRequest, res: Response) => {
    const body = req.body
    //console.log(body)
    const params = [body.meme_text, body.theme, req.userId]
    await dao.postMeme(params)
    res.status(200).send('Congrats, a new meme has been released...')
})

export default router