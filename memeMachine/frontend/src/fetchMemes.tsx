import { useState } from 'react'
import './styles/fetchMemes.css'
import { fetchProps } from './fetchPage'
import ostrich from './img/themes/angry-ostrich.jpg'
import llama from './img/themes/confused-llama.jpg'
import disappointedCat from './img/themes/disappointed-cat.jpg'
import skeletons from './img/themes/skeletons.jpg'
import thinkingCat from './img/themes/thinking-cat.jpg'

const getTheme = (theme: string) => {
    switch (theme) {
        case 'angry-ostrich':
            return <img src={ostrich} />
        case 'confused-llama':
            return <img src={llama} />
        case 'disappointed-cat':
            return <img src={disappointedCat} />
        case 'skeletons':
            return <img src={skeletons} />
        case 'thinking-cat':
            return <img src={thinkingCat} />
    }
}

export function FetchAllMemes({userToken, setMemes}: fetchProps) {


    const getAllMemes = async () => {
        const response = await fetch('/mememachine', {
            method: 'GET',
            headers: {'Authorization': `Bearer ${userToken}`}
        })

        const data = await response.json()
        if(setMemes !== undefined) {
            setMemes(data) 
        }
    }

    return <div>
        <button onClick={getAllMemes}>Hae kaikki meemit</button>
    </div>
}


export function FetchMemesByTheme({ userToken, setMemes }: fetchProps) {

    const getMemesByTheme = async (theme: string) => {
        const response = await fetch(`/mememachine/theme/${theme}`, {
            method: 'GET',
            headers: {'Authorization': `Bearer ${userToken}`}
        })
        const data = await response.json()
        if(setMemes !== undefined) {
            setMemes(data) 
        }
    }

    return <div>
        <ul>
            <li><button onClick={() => getMemesByTheme('angry-ostrich')}>Angry Ostrich</button></li>
            <li><button onClick={() => getMemesByTheme('confused-llama')}>Confused Llama</button></li>
            <li><button onClick={() => getMemesByTheme('disappointed-cat')}>Disappointed Cat</button></li>
            <li><button onClick={() => getMemesByTheme('skeletons')}>Skeletons</button></li>
            <li><button onClick={() => getMemesByTheme('thinking-cat')}>Thinking Cat</button></li>
        </ul>
    </div>
}


export function FetchLatestMemes({ userToken, setMemes }: fetchProps) {
    
    const getLatestMemes = async () => {
        const response = await fetch(`/mememachine/latestMemes`, {
            method: 'GET',
            headers: {'Authorization': `Bearer ${userToken}`}
        })
        const data = await response.json()
        if(setMemes !== undefined) {
            setMemes(data) 
        }
    }

    return <div>
        <button onClick={getLatestMemes}>Hae uusimmat meemit</button>
        </div>
}


//Tämä jatkokehitystä varten
export function FetchMemeById({ userToken }: fetchProps) {

    const [meme, setMeme] = useState({ id: 0, meme_text: "", meme_time: "", likes: 0, theme: "", user_id: 0 })
    const [meme_id, setMemeId] = useState(0)

    const getMemeById = async (meme_id: number) => {
        const response = await fetch(`/mememachine/id/${meme_id}`, {
            method: 'GET',
            headers: {'Authorization': `Bearer ${userToken}`}
        })

        const meme = await response.json()
        setMeme(meme)
    }

    return <div >
        <input onChange={(event) => setMemeId(Number(event.target.value))} />
        <button onClick={() => getMemeById(meme_id)}>Hae meemi</button>
        <div className='container-fetch'>
            <div className='displayBox'>
                <div className='meme'>
                    {getTheme(meme.theme)}
                </div>
                <div className='text'>
                    {meme.meme_text}
                </div>
            </div>
        </div>
    </div>
}