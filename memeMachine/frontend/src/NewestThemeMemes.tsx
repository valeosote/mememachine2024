/* eslint-disable @typescript-eslint/no-explicit-any */
import { useEffect, useState } from 'react'
import Theme1 from './img/themes/thinking-cat.jpg'
import Theme2 from './img/themes/confused-llama.jpg'
import './styles/MainPage.css'

const MainPage = () => {

  const [latestMeme1, setLatestMeme1] = useState<any>(null)
  const [latestMeme2, setLatestMeme2] = useState<any>(null)
  const selectedTheme1 = 'thinking-cat'
  const selectedTheme2 = 'confused-llama'

  useEffect(() => {
    fetchLatestMeme(selectedTheme1, setLatestMeme1)
  }, [selectedTheme1])

  useEffect(() => {
    fetchLatestMeme(selectedTheme2, setLatestMeme2)
  }, [selectedTheme2])

  const fetchLatestMeme = async (theme: string, setLatestMeme: any) => {
    try {
      const response = await fetch(`/mememachine/theme/latest/${theme}`)
      if (!response.ok) {
        throw new Error('Network response was not ok')
      }
      const data = await response.json()
      setLatestMeme(data)
    } catch (error) {
      console.error(`Error fetching latest meme from theme ${theme}:`, error)
    }
  }

  return (
    <>
      <div className='left'>
        {latestMeme1 && (
          <>
            <h5>Uusin meemi teemasta:</h5> <h2>{`${selectedTheme1}`}</h2>
            <img className='theme-image' src={Theme1} alt={`Theme ${selectedTheme1}`} />
            <p>'{latestMeme1.meme_text}'</p>
          </>
        )}
      </div>
      <div className='middle'>
        {latestMeme2 && (
          <>
            <h5>Uusin meemi teemasta:</h5><h2>{`${selectedTheme2}`}</h2>
            <img className='theme-image' src={Theme2} alt={`Theme ${selectedTheme2}`} />
            {latestMeme2 && latestMeme2.meme_text && (
              <p>'{latestMeme2.meme_text}'</p>
            )}
          </>
        )}
      </div>
    </>
  )
}

export default MainPage
