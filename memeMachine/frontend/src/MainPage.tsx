import NewestThemeMemes from './NewestThemeMemes'
import './styles/MainPage.css'

const MainPage = () => {

  return (
    <>
      <div className='container-mainpage'>
        <NewestThemeMemes />
      </div>
    </>
  )
}

export default MainPage
