import { useRouteError, Link, isRouteErrorResponse } from 'react-router-dom'

export default function ErrorPage() {
    const error = useRouteError()

    if (!isRouteErrorResponse(error)) {
        console.error(error)
        return (
            <div className='ErrorPage'>
                <h1>Odottamaton virhe!</h1>
                <p>Näin ei olisi kyllä pitänyt käydä...</p>
            </div>
        )
    }

    if (error.status === 404) return (
        <div className='ErrorPage'>
            <h1>404 - Ei löydy</h1>
            <p>Etsimääsi sivua ei ole.</p>
            <Link to={'/'}>Palaa pääsivulle</Link>
        </div>
    )

    console.error(error)
    return (
        <div className='ErrorPage'>
            <h1>Odottamaton virhe!</h1>
            <p>({error.status}) {error.statusText}</p>
            {error.data?.message
                ? <p>{error.data.message}</p>
                : null}
        </div>
    )
}