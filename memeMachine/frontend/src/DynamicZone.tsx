import { Dispatch, SetStateAction, useEffect, useState } from 'react'
import './styles/DynamicZone.css'

interface dynamicZoneProps {
  slider: number
  setSlider: Dispatch<SetStateAction<number>>
  username: string
  setUsername: Dispatch<SetStateAction<string>>
  setToken: Dispatch<SetStateAction<string>>
}
interface kaavakeProps {
  setGlobalUsername: Dispatch<SetStateAction<string>>
  setToken: Dispatch<SetStateAction<string>>
}
interface welcomeProps {
  setSlider: Dispatch<SetStateAction<number>>
}
interface familiarProps {
  username: string
}

export default function DynamicZone({ slider, setSlider, username, setUsername, setToken }: dynamicZoneProps) {

  useEffect(() => {
    setSlider(username === '' ? 0 : 3)
  }, [username, setSlider])

  const returnComponent = () => {
    switch (slider) {
      case 3:
        return <Familiar username={username}/>
      case 2:
        return <Signin setGlobalUsername={setUsername} setToken={setToken}/>
      case 1:
        return <Register setGlobalUsername={setUsername} setToken={setToken}/>
      case 0:
        return <Welcome setSlider={setSlider} />
      default:
    }
  }

  return (
    <>
      {returnComponent()}
    </>
  )
}

function Welcome({ setSlider }: welcomeProps) {

  return (
    <div className='container-dynamiczone'>
      <h2>Tervetuloa meemien ihmeelliseen maailmaan!</h2>
      <div className="container-form">
        <div className="row">
          <div className="left">
            Jos olet jo käyttäjä, kirjaudu.
            <br></br>
            <p>Muuten, rekisteröidy.</p>
          </div>
        </div>
        <div className="row">
          <div className="left">
            <button onClick={() => setSlider(1)}>Rekisteröidy</button>
            <button onClick={() => setSlider(2)}>Kirjaudu</button>
          </div>
        </div>
      </div>
    </div>
  )
}

function Familiar({username}: familiarProps) {

  return (
    <div className='container-dynamiczone'>
      <div>
        <h2>Terve vaan, {username}.</h2>
        <p>Valitse toiminto valikosta. Hyvää meemittelyä!</p>
      </div>
    </div>
  )
}

function Register({ setGlobalUsername, setToken }: kaavakeProps) {

  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')

  const isSubmitDisabled = !(username.length >= 2 && username.length <= 200 && password.length >= 6 && password.length <= 200)

  const handleSubmit = async (e: { preventDefault: () => void }) => {
    e.preventDefault()
    //console.log('Rekisteröidään: ', username, password)
    const response = await fetch('/mememachine/register', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ username, password }),
    })
    //console.log(response)
    if (response.ok) {
      const responseData = await response.json()
      const token = responseData.token
      //console.log('Token: ', token)
      setGlobalUsername(username)
      setToken(token)
      //setSlider(3) (tätä ei tarvitakaan kun useEffect päivittää sen usernamen vaihtuessa)
    } else {
      console.error('Registration failed:', response.status, response.statusText)
  }
}

  return (
    <div className='container-dynamiczone'>
      <h2>Rekisteröidy käyttäjäksi</h2>
      <div className="container-form">
        <div className="row">
          <div className="left">Nimi:</div>
          <div className="right"><input type="text" value={username} onChange={(e) => setUsername(e.target.value)} /></div>
        </div>
        <div className="row">
          <div className="left">Salasana:</div>
          <div className="right"><input type="password" value={password} onChange={(e) => setPassword(e.target.value)} /></div>
        </div>
        <div className="buttonrow">
          <div className="left"><button onClick={handleSubmit} disabled={isSubmitDisabled}>Rekisteröidy</button></div>
          <div className="right"></div>
        </div>
      </div>
    </div>
  )
}

function Signin({ setGlobalUsername, setToken }: kaavakeProps) {

  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')

  const isSubmitDisabled = !(username.length >= 2 && username.length <= 200 && password.length >= 6 && password.length <= 200)

  const handleSubmit = async (e: { preventDefault: () => void }) => {
    e.preventDefault()
    console.log('Kirjaudutaan: ', username, password)
    const response = await fetch('/mememachine/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ username, password }),
    })
    //console.log(response)
    if (response.ok) {
      const responseData = await response.json()
      const token = responseData.token
      //console.log('Token: ', token)
      setGlobalUsername(username)
      setToken(token)
      //setSlider(3)
    } else {
      console.error('Login failed:', response.status, response.statusText)
    }
  }
  return (
    <div className='container-dynamiczone'>
      <h2>Kirjaudu sisään</h2>
      <div className="container-form">
        <div className="row">
          <div className="left">Nimi:</div>
          <div className="right"><input type="text" value={username} onChange={(e) => setUsername(e.target.value)} /></div>
        </div>
        <div className="row">
          <div className="left">Salasana:</div>
          <div className="right"><input type="password" value={password} onChange={(e) => setPassword(e.target.value)} /></div>
        </div>
        <div className="buttonrow">
          <div className="left"><button onClick={handleSubmit} disabled={isSubmitDisabled}>Kirjaudu</button></div>
          <div className="right"></div>
        </div>
      </div>
    </div>
  )
}