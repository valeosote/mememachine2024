//import Header from './Header';
import MainPage from './MainPage'
import { useState } from 'react'
import DynamicZone from './DynamicZone'
import FetchPage from './fetchPage'
import PostMeme from './postMeme'
import Menu from './Menu'
import logo from './img/logo.png'
import './styles/App.css'

function App() {

  const [username, setUsername] = useState('')
  const [userToken, setUserToken] = useState('')
  const [slider, setSlider] = useState(0)
  const [showFetch, setShowFetch] = useState(false)
  const [showPost, setShowPost] = useState(false)
  const [hideHome, setHideHome] = useState(false)

  const handleLogout = () => {
    setUsername('')
    setUserToken('')
    setSlider(0)
    setHideHome(false)
    setShowFetch(false)
    setShowPost(false)
  }

  return (
    <div className='App'>
      <header className='header'>
        <div className='logo'>
          <img src={logo} alt='MemeMachine' />
        </div>
        <div className='navbar'>
          {username !== '' ? <Menu setShowFetch={setShowFetch} setShowPost={setShowPost} setHideHome={setHideHome} /> : <div className='homeButton' onClick={() => setSlider(0)}>Koti</div>}
        </div>
        {username !== '' ? <p id='loginuser'>Kirjautunut:  {username}</p> : ''}
        {username !== '' ? <button onClick={handleLogout}>Logout</button>  : ''}
      </header>

      <main className='main'>
        {hideHome ? '' :
          <div className='row'>
            <div className='column.left'>
              <MainPage />
            </div>
            <div className='column.right'>
              <DynamicZone slider={slider} setSlider={setSlider} username={username} setUsername={setUsername} setToken={setUserToken} />
            </div>
          </div>}
        {userToken !== '' ?
          <div>
            {showFetch ? <FetchPage userToken={userToken}/> : hideHome}
            {showPost ? <PostMeme userToken={userToken}/> : hideHome}
          </div>
          : ''}
      </main>
    </div>
  )
}

export default App