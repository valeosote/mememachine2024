import React from 'react'
import ReactDOM from 'react-dom/client'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import App from './App.tsx'
import ErrorPage from './ErrorPage'
/*import MainPage from './MainPage'
import FetchPage from './fetchPage.tsx'
import PostMeme from './postMeme.tsx'*/

const router = createBrowserRouter([
  {
      path: '/',
      element: <App />,
    errorElement: <ErrorPage />
  }
])

/*  //tässä alla olevassa versiossa olisi lapsia, joita ei vielä implementoitu
const router = createBrowserRouter([
  {
      path: '/',
      element: <App />,
      children: [{
        path: '/homepage/welcome',
        element: <MainPage />,
        errorElement: <ErrorPage />
      },{
        path: '/fetchpage',
        element: <FetchPage />,
        errorElement: <ErrorPage />
      },{
        path: '/postmeme',
        element: <PostMeme />,
        errorElement: <ErrorPage />
      }
      ],
    errorElement: <ErrorPage />
  }
])*/

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
)
