import { Dispatch, SetStateAction } from 'react'
import './styles/Header.css'
import './styles/Menu.css'

interface MenuProps {
    setShowFetch: Dispatch<SetStateAction<boolean>>
    setShowPost: Dispatch<SetStateAction<boolean>>
    setHideHome: Dispatch<SetStateAction<boolean>>
}

export default function Menu({ setShowFetch, setShowPost, setHideHome }: MenuProps) {

    const homeClicked = () => {
        setHideHome(false)
        setShowPost(false)
        setShowFetch(false)
    }

    const fetchMemes = () => {
        setShowFetch(true)
        setShowPost(false)
        setHideHome(true)
    }

    const postMemes = () => {
        setShowPost(true)
        setShowFetch(false)
        setHideHome(true)
    }

    return (
        <div className='topnav'>
            <div className='menu-link1' onClick={homeClicked}>Koti</div>
            <div className='menu-link2' onClick={fetchMemes}>Hae meemejä</div>
            <div className='menu-link3' onClick={postMemes}>Lisää meemi</div>
        </div>
    )
}