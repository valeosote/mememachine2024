import { FetchAllMemes, FetchMemesByTheme, FetchLatestMemes } from './fetchMemes'
import { useState, Dispatch, SetStateAction, useEffect } from 'react'
import ostrich from './img/themes/angry-ostrich.jpg'
import llama from './img/themes/confused-llama.jpg'
import disappointedCat from './img/themes/disappointed-cat.jpg'
import skeletons from './img/themes/skeletons.jpg'
import thinkingCat from './img/themes/thinking-cat.jpg'
import heart from './img/heart.png'
import emptyheart from './img/emptyheart.png'
import './styles/fetchPage.css'


interface Meme {
    meme_id: number,
    meme_text: string,
    meme_time: Date,
    likes: number,
    theme: string,
    user_id: number,
    liked?: boolean
}

const getTheme = (theme: string) => {
    switch (theme) {
        case 'angry-ostrich':
            return <img className='theme-image' src={ostrich} />
        case 'confused-llama':
            return <img className='theme-image' src={llama} />
        case 'disappointed-cat':
            return <img className='theme-image' src={disappointedCat} />
        case 'skeletons':
            return <img className='theme-image' src={skeletons} />
        case 'thinking-cat':
            return <img className='theme-image' src={thinkingCat} />
    }
}


export interface fetchProps {
    userToken: string,
    setMemes?: Dispatch<SetStateAction<Meme[]>>
}

export default function FetchPage({ userToken }: fetchProps) {

    const [memes, setMemes] = useState<Array<Meme>>([])
    const [memesWithLikes, setMemesWithLikes] = useState<Array<Meme>>([])

    useEffect(() => {
        setMemesWithLikes(memes.map(obj => ({...obj, liked: false})))
    }, [memes])
    
    const handleLike = async (memeId: number) => {
        try {
            const response = await fetch(`/mememachine/id/like/${memeId}`, {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${userToken}`,
                    'Content-Type': 'application/json'
                },
            })
            if (!response.ok) {
                throw new Error('Failed to like meme');
            }
            setMemesWithLikes(prevMemes => prevMemes.map(meme =>
                meme.meme_id === memeId
                    ? { ...meme, liked: true, likes: meme.likes + 1 }
                    : meme
            )) // Updates the likes count and marks the single meme as liked (which resets once the meme is fetched from the db)

            //setMemes(memes.map(meme => meme.meme_id === memeId ? { ...meme, likes: meme.likes + 1 } : meme)) // Updates the likes count
        } catch (error) {
            console.error('Error liking meme:', error)
        }
    }

    return <div>
        <h1>Hae meemejä</h1>
        <div className='row'>
            <div className='content'>
                <h4>Valitse teema:</h4>
                <FetchMemesByTheme userToken={userToken} setMemes={setMemes} />
            </div>
            <div className='content'>
                <h4>Tai:</h4>
                <FetchLatestMemes userToken={userToken} setMemes={setMemes} />
                <FetchAllMemes userToken={userToken} setMemes={setMemes} />
            </div>
        </div>
        <div className='container-fetch'>
            {memesWithLikes.map(meme =>
                <div className='displayBox'>
                    <div className='meme-image'>
                        {getTheme(meme.theme)}
                    </div>
                    <div className='text'>
                        {meme.meme_text}
                    </div>
                    <div className='likes-container'>
                        {/* Display the number of likes */}
                        <div className='likes'>Likes: {meme.likes}</div>
                        {/* Like button */}
                        <button className='button-like' onClick={() => handleLike(meme.meme_id)} disabled={meme.liked}>
                            <img className='heart' src={meme.liked ? heart : emptyheart} alt="Like this meme!" />
                        </button>
                    </div>
                </div>)}
        </div>
    </div>
}