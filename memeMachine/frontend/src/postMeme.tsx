import './styles/postMeme.css'
import ostrich from './img/themes/angry-ostrich.jpg'
import llama from './img/themes/confused-llama.jpg'
import disappointedCat from './img/themes/disappointed-cat.jpg'
import skeletons from './img/themes/skeletons.jpg'
import thinkingCat from './img/themes/thinking-cat.jpg'
import { useState } from 'react'

interface Post {
  userToken: string
}

function PostMeme({ userToken }: Post) {

    const [theme, setTheme] = useState('')
    const [input, setInput] = useState('')

    const isPostDisabled = (theme==='' && input==='')

    const pickATheme = (pic: string) => {
        setTheme(pic)
        
    }

    const submitNewMeme = () => {

        fetch('/mememachine/post', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${userToken}`
          },
          body: JSON.stringify({
            meme_text: input,
            theme: theme,
          })
        })
        setInput('')
      }

    const chosenImg = (theme: string) => {
      if (theme !== undefined)
      switch (theme) {
        case 'angry-ostrich':
            return ostrich
        case 'confused-llama':
            return llama
        case 'disappointed-cat':
            return disappointedCat
        case 'skeletons':
            return skeletons
        case 'thinking-cat':
            return thinkingCat
    }
    else {
      return './img/themes/angry-ostrich.jpg'
    }
    }

    return (
        <div className='container-postMeme'>
            <h1>Lisää uusi meemi</h1>
            <p>Aloita valitsemalla kuva - lisää teksti - Unleash the MEME!!!</p>
            <div className='gallery'>
                    <div className='theme'><img src={ostrich} onClick={() => pickATheme('angry-ostrich')}/></div>
                    <div className='theme'><img src={llama} onClick={() => pickATheme('confused-llama')} /></div>
                    <div className='theme'><img src={disappointedCat} onClick={() => pickATheme('disappointed-cat')} /></div>
                    <div className='theme'><img src={skeletons} onClick={() => pickATheme('skeletons')} /></div>
                    <div className='theme'><img src={thinkingCat} onClick={() => pickATheme('thinking-cat')} /></div>
            </div>
        <div className='memeText'>
          <div id='newmeme-left'>
            <br></br>
            <p>Valitsemasi kuva: </p>
            <img src={chosenImg(theme)} />
          </div>
          <div id='newmeme-right'>
            <textarea value={input} onChange={e => { setInput(e.target.value) }} placeholder="Pistäppä meemis tähän..."></textarea>
            <br></br>
            <button id='button-newmeme'onClick={submitNewMeme} disabled={isPostDisabled}>Lähetä</button>
          </div>
        </div>
      </div>
    )
}

export default PostMeme